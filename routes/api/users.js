const express = require('express');
const router = express.Router();
let users = require('../../users/users.json');

function compare( a, b ) {
  if ( a.name < b.name ){
    return -1;
  }
  if ( a.name > b.name ){
    return 1;
  }
  return 0;
}  


// GET all users
router.get('/', (req, res) => {
  res.json(users.users.sort(compare));
});


module.exports = router;