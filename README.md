# Binar FSW Chapter 5 - viandikafauzi

### Features:
  - Landing page
  - Trial game (rock, paper, scissor)
  - Users API access using static file


### Tech

This app uses a number of open source projects to work properly:

* HTML
* CSS
* Javascript
* [Boostrap](https://getbootstrap.com)
* [Node.JS](https://nodejs.dev)
* [Express](https://expressjs.com)

### Installation

Requires [Node.JS](https://nodejs.org/) to run.

Install the dependencies and start the server.

```sh
$ npm install
$ npm start
```

Verify the deployment by navigating to your server address in your preferred browser. The terminal will let you know which port the apps is on. If there's no system env specify, it will run on port 5000 automatically.

```sh
127.0.0.1:5000
```

Enjoy!