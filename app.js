const express = require("express");
const path = require("path");
const users = require("./users/users.json");

const app = express();

// Static folder
app.use(express.static("public"));

// Body Parser Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Express routing
app.get("/chapter3", (req, res) => {
  res.sendFile(path.join(__dirname, "public", "index.html"));
});

app.get("/chapter4", (req, res) => {
  res.sendFile(path.join(__dirname, "public", "batuguntingkertas.html"));
});


// Users API Routes
app.use('/api/users', require('./routes/api/users'));

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
